-- Original Screenplay Written by: TheTinyPebble
-- Adapted by Alpha 2023

-- Boss States
-- 1: Unshifted Anakin
-- 2: Anakin Red Saber
-- 3: Vader
-- 5: Crystals destroyed and visions defeated.

--[[TODO:
	Add broadcast messages
	Add start encounter mechanic
	Add crystal objects
	Add theater [Not done server side]
	Add player scaling
	Add finish encounter function
		Delete data bytes on completion
]]

visionsFuture2 = ScreenPlay:new {
	scriptName = "visionsFuture2",
	planetName = "corellia",
	x = 536,
	z = 3,
	y = -6572,

	bossShapes = { 
		"world_anakin_skywalker_emperor",
		"world_padme",
	},

	chokeCone = 60,
	poisonCloudRadius = 5,

	radius = 25,
	
	shuttleCleanup = 60 * 60, -- Automatic shuttle cleanup after 1 hours if the shuttle has not been told to lift off
}

registerScreenPlay("visionsFuture2", false)

function visionsFuture2:start()
	-- Clear it out first
	self:cleanUp()
	self:cleanupScene()
	createEvent(10 * 1000, self.scriptName, "startEncounter", nil, "")
end

function visionsFuture2:stop()
	writeData("visionsFuture2:bossState", 99)
	local pBoss = getSceneObject(readData("visionsFuture2:bossID"))
	self:cleanUp(pBoss)
	createEvent(1 * 1000, self.scriptName, "cleanupScene", nil, "")
end

function visionsFuture2:startEncounter()
	-- Clear it out first
	self:cleanupScene()
	
	broadcastGalaxy("all", "There is a great ripple in the force in Hunter's Retreat on the planet Corellia.")

	local pBoss = spawnMobile(self.planetName, self.bossShapes[1], 0, self.x, self.z, self.y, math.random(1, 360), 0)
	writeData("visionsFuture2:bossID", SceneObject(pBoss):getObjectID())
	writeData("visionsFuture2:bossState", 1)
	createObserver(OBJECTDESTRUCTION, self.scriptName, "notifyAnakinDestroyed", pBoss)

	self:storeObjectID(pBoss)
	
	local pBoss2 = spawnMobile(self.planetName, self.bossShapes[2], 0, self.x+5, self.z, self.y+5, math.random(1, 360), 0)
	writeData("visionsFuture2:boss2ID", SceneObject(pBoss2):getObjectID())
	writeData("visionsFuture2:boss2State", 1)
	createObserver(OBJECTDESTRUCTION, self.scriptName, "notifyPadmeDestroyed", pBoss2)

	self:storeObjectID(pBoss2)

	writeData("visionsFuture2:bossState:anakin", 1)
	writeData("visionsFuture2:bossState:padme", 1)

	createEvent(math.random(5, 6) * 1000, self.scriptName, "doBossMechanic", nil, "")
	createEvent(math.random(5, 6) * 1000, self.scriptName, "doAddsMechanic", nil, "")
	self:setupMusic(pBoss)
	spatialMoodChat(pBoss2, "Come peacefully and you will not be harmed.", "12", "4")
	writeData("visionsFuture2:bombState", 0)
end

---- Boss Mechanics
function visionsFuture2:doBossMechanic()
	local pBoss = getSceneObject(readData("visionsFuture2:bossID"))
	local pBoss2 = getSceneObject(readData("visionsFuture2:boss2ID"))
	local bossState = readData("visionsFuture2:bossState")

	if ((pBoss == nil or CreatureObject(pBoss):isDead()) and (pBoss2 == nil or CreatureObject(pBoss2):isDead())) then
		print("Boss Null or Dead stopping mechanics")
		return
	end

	if (bossState == 1) then
		self:doGrenadeMechanic(pBoss2)
		local boss2State = readData("visionsFuture2:bossState:padme")
		if (boss2State == 0) then
			self:doChokeMechanic(pBoss)
		end
	end
		
	if (bossState == 1) then
		createEvent(math.random(15, 20) * 1000, self.scriptName, "doBossMechanic", nil, "")
	end
end

---- Boss Mechanics
function visionsFuture2:doAddsMechanic()
	local pBoss = getSceneObject(readData("visionsFuture2:bossID"))
	local pBoss2 = getSceneObject(readData("visionsFuture2:boss2ID"))
	local bossState = readData("visionsFuture2:bossState")

	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		print("Boss Null or Dead stopping add mechanic")
		return
	end

	if (bossState == 1) then
		self:doFlyby(pBoss)
	end
	
	if (bossState == 1) then
		createEvent(math.random(60, 90) * 1000, self.scriptName, "doAddsMechanic", nil, "")
	end
end

function visionsFuture2:doGustMechanic(pBoss)
	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		return
	end
	
	spatialMoodChat(pBoss, "Stay Down!", "18", "80") --shouts confidently

	local playerTable = SceneObject(pBoss):getPlayersInRange(60)

	if (playerTable == nil) then
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]

		if (pPlayer ~= nil) then
			if (not CreatureObject(pPlayer):isDead()) then
				CreatureObject(pPlayer):sendSystemMessage("With a brief motion Anakin sends everyone flying with the force.")
				CreatureObject(pPlayer):setPosture(KNOCKEDDOWN)
			end
		end
		for j = 0, 6, 3 do
			CreatureObject(pPlayer):inflictDamage(pBoss, j, CreatureObject(pPlayer):getMaxHAM(j) * 0.1, 1)
		end
	end
end

function visionsFuture2:doGrenadeMechanic(pBoss2)
	if (pBoss2 == nil or CreatureObject(pBoss2):isDead()) then
		return
	end
		
	spatialMoodChat(pBoss2, "Take this!", "18", "80") --shouts confidently

	local playerTable = SceneObject(pBoss2):getPlayersInRange(32)

	if (playerTable == nil) then
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]

		if (pPlayer ~= nil) then
			if (not CreatureObject(pPlayer):isDead()) then
				CreatureObject(pPlayer):sendSystemMessage("Padme hurls a grenade at you.")
				CreatureObject(pPlayer):setPosture(KNOCKEDDOWN)
				CreatureObject(pPlayer):playEffect("clienteffect/combat_grenade_cryoban.cef", "")
			end
		end
		for j = 0, 6, 3 do
			CreatureObject(pPlayer):inflictDamage(pBoss2, j, CreatureObject(pPlayer):getMaxHAM(j) * 0.15, 1)
		end
	end
end

function visionsFuture2:doChokeMechanic(pBoss)
	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		return
	end
	
	spatialMoodChat(pBoss, "You will die.", "24", "80") --admits cruelly

	local angleStart = SceneObject(pBoss):getDirectionAngle() - self.chokeCone / 2

	if (angleStart < 0) then
		angleStart = 360 + angleStart
	end

	local angleEnd = angleStart + self.chokeCone

	if (angleEnd > 360) then
		angleEnd = self.chokeCone - (360 - angleStart)
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(30)

	if (playerTable == nil) then
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]
		local x1 = CreatureObject(pBoss):getWorldPositionX()
		local y1 = CreatureObject(pBoss):getWorldPositionY()
		local x2 = CreatureObject(pPlayer):getWorldPositionX()
		local y2 = CreatureObject(pPlayer):getWorldPositionY()

		local angle = math.atan2(x2 - x1, y2 - y1) * 180 / math.pi

		if (angle < 0) then
			angle = 360 + angle
		end

		if (angle >= angleStart and angle <= angleEnd) then
			for j = 0, 6, 3 do
				CreatureObject(pPlayer):inflictDamage(pBoss, j, CreatureObject(pPlayer):getMaxHAM(j) * 0.2, 1)
			end
			CreatureObject(pPlayer):addDotState(pBoss, BLEEDING, 2000, HEALTH, 10, 100, SceneObject(pBoss):getObjectID(), 0)
			CreatureObject(pPlayer):addDotState(pBoss, BLEEDING, 2000, ACTION, 10, 100, SceneObject(pBoss):getObjectID(), 0)
			CreatureObject(pPlayer):addDotState(pBoss, BLEEDING, 2000, MIND, 10, 100, SceneObject(pBoss):getObjectID(), 0)
			CreatureObject(pPlayer):sendSystemMessage("The dark figure uses the force to choke the life from all those in his gaze.")
		end
	end
end

function visionsFuture2:doCloudMechanic(pBoss)
	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		return
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(64)

	if (playerTable == nil) then
		return
	end

	local shuffledPlayerTable = self:shuffleTable(playerTable)

	for i = 1, math.ceil(#shuffledPlayerTable / 5), 1 do
		local pPlayer = shuffledPlayerTable[i]
		self:createCloud(pPlayer)
		CreatureObject(pPlayer):playEffect("clienteffect/combat_explosion_lair_large.cef", "")
		CreatureObject(pPlayer):sendSystemMessage("The approaching Gunship fires on you as it approaches to land.")
	end
end

function visionsFuture2:createCloud(pPlayer)
	if (pPlayer == nil) then
		return
	end

	local xPos = CreatureObject(pPlayer):getWorldPositionX()
	local yPos = CreatureObject(pPlayer):getWorldPositionY()
	local zPos = getTerrainHeight(pPlayer, xPos, yPos)

	local pSceneObject = spawnSceneObject(self.planetName, "object/static/particle/pt_lair_evil_fire_large_red.iff", xPos, zPos, yPos, 0, 0)
	createEvent(30 * 1000, self.scriptName, "destroyObject", pSceneObject, "")
	createEvent(3 * 1000, self.scriptName, "doCloudDamage", pSceneObject, "") --3 seconds to get out of fire.
end

function visionsFuture2:doCloudDamage(pSceneObject)
	if (pSceneObject == nil) then
		return
	end

	local playerTable = SceneObject(pSceneObject):getPlayersInRange(self.poisonCloudRadius)

	if (playerTable == nil) then
		createEvent(3 * 1000, self.scriptName, "doCloudDamage", pSceneObject, "")
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]
		local curDist = SceneObject(pPlayer):getDistanceTo(pSceneObject)

		if (curDist <= self.poisonCloudRadius) then
			CreatureObject(pPlayer):addDotState(pSceneObject, ONFIRE, 750, HEALTH, 60, 100, SceneObject(pSceneObject):getObjectID(), 0)
			CreatureObject(pPlayer):addDotState(pSceneObject, ONFIRE, 750, ACTION, 60, 100, SceneObject(pSceneObject):getObjectID(), 0)
			CreatureObject(pPlayer):addDotState(pSceneObject, ONFIRE, 100, MIND, 60, 100, SceneObject(pSceneObject):getObjectID(), 0)
		end
	end

	createEvent(3 * 1000, self.scriptName, "doCloudDamage", pSceneObject, "")
end

---- Healing Mechanic
function visionsFuture2:checkKillingPulse(tracker, pAddID)
	local pAdd = getSceneObject(pAddID)
	local pBoss = getSceneObject(readData("visionsFuture2:bossID"))
	local pBoss2 = getSceneObject(readData("visionsFuture2:boss2ID"))
	
	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		pBoss = pBoss2
	end
	
	if (pAddID == nil) then
		return
	end
	
	local bombState = readData("visionsFuture2:bombState")
	
	if (bombState == 1) then
		CreatureObject(pAdd):inflictDamage(pBoss, 0, 100000, 1)
		return
	end

	createEvent(2 * 1000, self.scriptName, "checkKillingPulse", nil, pAddID)
end

function visionsFuture2:healBoss(pBoss)
	if (pBoss == nil) then
		return
	end

	for i = 0, 6, 3 do
		local maxHAM = CreatureObject(pBoss):getMaxHAM(i)
		CreatureObject(pBoss):setWounds(i, 0)
		CreatureObject(pBoss):setShockWounds(i, 0)
		CreatureObject(pBoss):setHAM(i, maxHAM)
	end
end

function visionsFuture2:notifyAnakinDestroyed(pBoss, bossNo)
	if (pBoss == nil) then
		return 0
	end

	writeData("visionsFuture2:bossState:anakin", 0)
	
	self:cleanUp(pBoss)

	local boss2State = readData("visionsFuture2:bossState:padme")

	if (boss2State == 0) then
		writeData("visionsFuture2:bossState", 99)
		self:cleanUp(pBoss)
		writeData("visionsFuture2:bombState", 1)
		createEvent(3 * 1000, self.scriptName, "cleanupScene", nil, "")
		self:giveAwards(pBoss)
		return 1
	end
	
	self:destroyObject(pBoss)
	
	local pBoss2 = getSceneObject(readData("visionsFuture2:boss2ID"))
	spatialMoodChat(pBoss2, "No!", "35", "40") --exlaims dismayed
	
	createEvent(10 * 1000, self.scriptName, "doBombMechanic", nil, "")

	local playerTable = SceneObject(pBoss2):getPlayersInRange(120)

	if (playerTable == nil) then
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]

		if (pPlayer ~= nil) then
			if (not CreatureObject(pPlayer):isDead()) then
				CreatureObject(pPlayer):sendSystemMessage("Padme calls in a baradium bomb on her own position. Defeat her before it destroys you all!")
			end
		end
	end
	return 1
end

function visionsFuture2:notifyPadmeDestroyed(pBoss2, bossNo)
	if (pBoss2 == nil) then
		return 0
	end

	writeData("visionsFuture2:bossState:padme", 0)
	
	local boss1State = readData("visionsFuture2:bossState:anakin")

	if (boss1State == 0) then
		writeData("visionsFuture2:bossState", 99)
		self:cleanUp(pBoss2)
		writeData("visionsFuture2:bombState", 1)
		createEvent(3 * 1000, self.scriptName, "cleanupScene", nil, "")
		self:giveAwards(pBoss2)
		return 1
	end
	
	self:destroyObject(pBoss2)
	
	local pBoss = getSceneObject(readData("visionsFuture2:bossID"))
	spatialMoodChat(pBoss, "Padme!", "35", "40") --exlaims dismayed
	
	for i = 0, 6, 3 do
		local maxHAM = CreatureObject(pBoss):getMaxHAM(i)
		CreatureObject(pBoss):setWounds(i, 0)
		CreatureObject(pBoss):setShockWounds(i, 0)
		CreatureObject(pBoss):inflictDamage(pBoss, i, -300000, 1)
	end
	
	createEvent(240 * 1000, self.scriptName, "doBombMechanic", nil, "")

	local playerTable = SceneObject(pBoss):getPlayersInRange(120)

	if (playerTable == nil) then
		return
	end
	
	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]

		if (pPlayer ~= nil) then
			if (not CreatureObject(pPlayer):isDead()) then
				CreatureObject(pPlayer):sendSystemMessage("Anakin calls in a baradium bomb on his own position. Defeat him before it destroys you all!")
			end
		end
	end
	
	return 1
end

---- Utilities
-- Cleanup
function visionsFuture2:destroyObject(pObject)
	if (pObject == nil) then
		return
	end

	if (SceneObject(pObject):isAiAgent()) then
		CreatureObject(pObject):setPvpStatusBitmask(0)
		forcePeace(pObject)
	end
	SceneObject(pObject):destroyObjectFromWorld()
end

function visionsFuture2:cleanupScene()
	local spawnedObjects = readStringData("visionsFuture2:spawnedObjects")
	local spawnedObjectsTable = HelperFuncs:splitString(spawnedObjects, ",")
	for i = 1, #spawnedObjectsTable, 1 do
		local pObject = getSceneObject(tonumber(spawnedObjectsTable[i]))
		if (pObject ~= nil) then
			if (SceneObject(pObject):isAiAgent()) then
				CreatureObject(pObject):setPvpStatusBitmask(0)
				forcePeace(pObject)
			end
			SceneObject(pObject):destroyObjectFromWorld()
		end
	end

	deleteStringData("visionsFuture2:spawnedObjects")
	deleteData("visionsFuture2:bossState")
	deleteData("visionsFuture2:boss2State")
	deleteData("visionsFuture2:bossID")
	deleteData("visionsFuture2:boss2ID")
	deleteData("visionsFuture2:bossState:anakin")
	deleteData("visionsFuture2:bossState:padme")
	deleteData("visionsFuture2:bombState")
end

-- Store spawned objects
function visionsFuture2:storeObjectID(pObject)
	if (pObject == nil) then
		return
	end
	local objectID = SceneObject(pObject):getObjectID()
	writeStringData("visionsFuture2:spawnedObjects", readStringData("visionsFuture2:spawnedObjects") .. "," .. objectID)
end

-- Fisher-Yates shuffle
function visionsFuture2:shuffleTable(tInput)
	math.randomseed(os.time())
	local tReturn = {}
	for i = #tInput, 1, -1 do
		local j = math.random(i)
		tInput[i], tInput[j] = tInput[j], tInput[i]
		table.insert(tReturn, tInput[i])
	end
	return tReturn
end

-- Calculating evenly spaced points on the perimeter of a circle
function visionsFuture2:getCircleSpawnPoints(centerX, centerY, radius, totalPoints)
	local pivotX, pivotY
	local spawnPoints = {}
	local theta = ((math.pi * 2) / totalPoints);

	for i = 1, totalPoints do
		local angle = theta * i
		pivotX = radius * math.cos(angle) + centerX;
		pivotY = radius * math.sin(angle) + centerY;
		table.insert(spawnPoints, {x = pivotX, y = pivotY})
	end
	return spawnPoints
end

-- Boss state logic
function visionsFuture2:bossStateLogic(pBoss, percent, stateString, checkState)
	if (pBoss == nil) then
		return false
	end

	if (readData(stateString) == checkState) then
		for i = 0, 6, 3 do
			if (CreatureObject(pBoss):getHAM(i) <= CreatureObject(pBoss):getMaxHAM(i) * percent and CreatureObject(pBoss):getHAM(i) >= CreatureObject(pBoss):getMaxHAM(i) * 0.1) then
				return true
			end
		end
	end
	return false
end

function visionsFuture2:setupMusic(pBoss)
	if (pBoss == nil) then
		return
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(250)
	local musicTemplate = ""
	local bossState = readData("visionsFuture2:bossState")
			
	if (bossState == 1) then
		musicTemplate = "sound/visions2_01.snd"
	elseif (bossState == 2) then
		musicTemplate = "sound/visions_02.snd"
	elseif (bossState == 3) then
		musicTemplate = "sound/visions_03.snd"
	elseif (bossState == 5) then
		musicTemplate = "sound/visions_01.snd"
	end
	

	if (#playerTable > 0) then
		for i = 0, #playerTable, 1 do
			local pPlayer = playerTable[i]

			if (pPlayer ~= nil and musicTemplate ~= nil) then
				CreatureObject(pPlayer):playMusicMessage(musicTemplate)
			end
		end
	end
end

function visionsFuture2:giveAwards(pBoss)
	if (pBoss == nil) then
		return
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(250)
	local musicTemplate = "sound/visions_05.snd"

	local rngloot = 0
	
	if (#playerTable > 0) then
		for i = 0, #playerTable, 1 do
			local pPlayer = playerTable[i]
			
			if (pPlayer ~= nil and musicTemplate ~= nil) then
				CreatureObject(pPlayer):playMusicMessage(musicTemplate)
				
				local pGhost = CreatureObject(pPlayer):getPlayerObject()
				if (pGhost ~= nil and not PlayerObject(pGhost):hasBadge(240)) then
					print("Awarding Badge")
					PlayerObject(pGhost):awardBadge(168)
					PlayerObject(pGhost):awardBadge(240)
				end
				
				local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")
				
				rngloot = 0 + getRandomNumber(0, 6)
				print (rngloot)

				if (pInventory == nil or SceneObject(pInventory):isContainerFullRecursive()) then
					print("Inventory Full")
					CreatureObject(pPlayer):sendSystemMessage("Your inventory is full so you did not receive the event reward.")
				else
					if rngloot == 0 or rngloot == 6 then
					local crystalID = createLoot(pInventory, "color_crystals", 300, true)
						local pCrystal = getSceneObject(crystalID)

						if (pCrystal == nil) then
							Logger:log("Crystal is nil. Unable to set Kun's Blood Crystal Color for Player ID: " .. SceneObject(pPlayer):getObjectID(), LT_ERROR)
							CreatureObject(pPlayer):sendSystemMessage("There was an error generating your Crystal Reward. Please see Support and screenshot this message.")
						else
							local colorCrystal = LuaLightsaberCrystalComponent(pCrystal)
							colorCrystal:setColor(39)
							colorCrystal:updateCrystal(39)
							CreatureObject(pPlayer):sendSystemMessage("A mysterious red crystal appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
						end
					end
					if rngloot == 1 then
						local crystalID = createLoot(pInventory, "krayt_tissue_rare", 300, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious tissue appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 2 then
						local crystalID = createLoot(pInventory, "power_crystals", 300, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious crystal appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 3 then
						local crystalID = createLoot(pInventory, "fire_breathing_spider", 110, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious object appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 4 then
						local crystalID = createLoot(pInventory, "jedi_paintings", 1, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious painting appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 5 then
						local crystalID = createLoot(pInventory, "dc15event", 1, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious schematic appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end		
				end

			end
			
		end
	end
end

function visionsFuture2:spawnShuttle(pBoss)
	local playerID = SceneObject(pBoss):getObjectID()
		
	local xPos = CreatureObject(pBoss):getWorldPositionX()
	local yPos = CreatureObject(pBoss):getWorldPositionY()
	local zPos = getTerrainHeight(pPlayer, xPos, yPos)
	xPos = (math.floor(xPos+0.5))
	yPos = (math.floor(yPos+0.5))
	zPos = (math.floor(yPos+0.5))
	writeData("visionsFuture2:spawnPointX", xPos)
	writeData("visionsFuture2:spawnPointZ", 28)
	writeData("visionsFuture2:spawnPointY", yPos)
	writeStringData("visionsFuture2:spawnTemplate", "world_clonetrooper_501")
	writeData("visionsFuture2:spawnLevel", 100)
	writeData("visionsFuture2:numSpawns", 10)
	writeData("visionsFuture2:heading", 92)
	writeStringData("visionsFuture2:spawnPlanet", self.planetName)

	local type = "object/creature/npc/theme_park/player_republic_gunship.iff"

	local heading = "325"
	local planet = readStringData("visionsFuture2:spawnPlanet")

	local pShuttle = spawnSceneObject(planet, type, xPos, zPos, yPos, 0, heading)

	if (pShuttle == nil) then
		print("Error creating shuttle. Please try again.")
		return nil
	end
	
	CreatureObject(pShuttle):setPosture(PRONE)

	CreatureObject(pShuttle):setCustomObjectName("Gunship")
	
	writeData("visionsFuture2:shuttleID", SceneObject(pShuttle):getObjectID())

	writeData("visionsFuture2:shuttleStatus", 1) -- Spawned
	
	return pShuttle
end

function visionsFuture2:doFlyby(pBoss)
	local pShuttle = self:spawnShuttle(pBoss)

	if (pShuttle == nil) then
		print("Error executing flyby, shuttle not found.")
		return
	end

	spatialMoodChat(pBoss, "Reinforcements on my position.", "18", "1") --shouts confidently

	createEvent(6 * 1000, self.scriptName, "landShuttle", pBoss, "")
	createEvent(29 * 1000, self.scriptName, "doTakeOff", pBoss, "")
	
	createEvent(22 * 1000, self.scriptName, "doCloudMechanic", pBoss, "")
	createEvent(23 * 1000, self.scriptName, "doCloudMechanic", pBoss, "")
end

function visionsFuture2:landShuttle(pBoss)
	local shuttleID = readData("visionsFuture2:shuttleID")
	local type = "Gunship"
	local pShuttle = getSceneObject(shuttleID)

	if (pShuttle == nil) then
		print("Error executing shuttle landing, shuttle not found.")
		return
	end

	CreatureObject(pShuttle):setPosture(UPRIGHT)

	writeData("visionsFuture2:shuttleStatus", 2) -- Landing
	createEvent(22 * 1000, self.scriptName, "dropOffNpcs", pBoss, "")
end

function visionsFuture2:dropOffNpcs(pBoss)
	if (pBoss == nil) then
		return
	end

	local posX = readData("visionsFuture2:spawnPointX")
	local posZ = readData("visionsFuture2:spawnPointZ")
	local posY = readData("visionsFuture2:spawnPointY")
	local template = readStringData("visionsFuture2:spawnTemplate")
	local mobLevel = readData("visionsFuture2:spawnLevel")
	local numSpawns = readData("visionsFuture2:numSpawns")
	local heading = readData("visionsFuture2:heading")
	local planet = readStringData("visionsFuture2:spawnPlanet")

	writeData("ShuttleDropoff:shuttleStatus", 3) -- Landed
	
	local boss2State = readData("visionsFuture2:bossState:padme")
	local bossState = readData("visionsFuture2:bossState:anakin")

	if (boss2State == 0  and bossState ~= 0) then
		template = "world_stormtrooper"
		numSpawns = numSpawns + 5
		local pCaptain = spawnEventMobile(planet, "world_clonetrooper_captain_501", mobLevel, posX, posZ, posY, heading, 0)
		local pCaptainID = SceneObject(pCaptain):getObjectID()
		createEvent(2 * 1000, self.scriptName, "checkKillingPulse", nil, pCaptainID)
	end
	
	local pAdd = nil
	local pAddID = nil

	for i = 1, numSpawns, 1 do
		local xOffset = -3 + getRandomNumber(0, 6)
		local yOffset = -3 + getRandomNumber(0, 6)
		pAdd = spawnEventMobile(planet, template, mobLevel, posX + xOffset, posZ, posY + yOffset, heading, 0)
		pAddID = SceneObject(pAdd):getObjectID()
		createEvent(2 * 1000, self.scriptName, "checkKillingPulse", nil, pAddID)
	end
end

function visionsFuture2:doTakeOff(pBoss)
	local shuttleID = readData("visionsFuture2:shuttleID")
	local type = "Gunship"
	local pShuttle = getSceneObject(shuttleID)

	if (pShuttle == nil) then
		print("Error executing shuttle take off, shuttle not found.")
		return
	end

	CreatureObject(pShuttle):setPosture(PRONE)

	writeData("visionsFuture2:shuttleStatus", 4) -- Taking off
	createEvent(14 * 1000, self.scriptName, "cleanUp", pBoss, "")
end

function visionsFuture2:destroyShuttle(pPlayer)

	local shuttleID = readData("visionsFuture2:shuttleID")
	local pShuttle = getSceneObject(shuttleID)

	if (pShuttle ~= nil) then
		SceneObject(pShuttle):destroyObjectFromWorld()
	end

	deleteData("visionsFuture2:shuttleID")
end

function visionsFuture2:cleanUp(pShuttle)
	print("Despawn SHuttle")
	if (pShuttle == nil) then
		print("shuttle nil")
		return
	end

	self:resetDefaults(pShuttle)
	self:destroyShuttle(pShuttle)
end

function visionsFuture2:resetDefaults(pPlayer)

	deleteData("visionsFuture2:setupStep")
	deleteData("visionsFuture2:setupCompleted")
	deleteData("visionsFuture2:shuttleType")
	deleteData("visionsFuture2:heading")
	deleteData("visionsFuture2:numSpawns")
	deleteData("visionsFuture2:spawnPointX")
	deleteData("visionsFuture2:spawnPointZ")
	deleteData("visionsFuture2:spawnPointY")
	deleteData("visionsFuture2:shuttleStatus")
	deleteData("visionsFuture2:spawnLevel")

	deleteStringData("visionsFuture2:spawnTemplate")
	deleteStringData("visionsFuture2:spawnPlanet")
	print("Despawn Complete")
end

function visionsFuture2:doBombMechanic()
	local pBoss = getSceneObject(readData("visionsFuture2:bossID"))
	local pBoss2 = getSceneObject(readData("visionsFuture2:boss2ID"))
	local boss2State = readData("visionsFuture2:bossState:padme")
	local bossState = readData("visionsFuture2:bossState:anakin")
	
	if (bossState == 0 and boss2State == 0) then
		print("Both bosses dead. No bomb.")
		return
	end
	
	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		pBoss = pBoss2
	end
	
	spatialMoodChat(pBoss, "The end at last.", "6", "24") --concludes bitterly
	
	writeData("visionsFuture2:bombState", 1)

	local playerTable = SceneObject(pBoss):getPlayersInRange(100)

	if (playerTable == nil) then
		return
	end
	
	CreatureObject(pBoss):playEffect("clienteffect/restuss_event_big_explosion.cef", "")

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]

		if (pPlayer ~= nil) then
			if (not CreatureObject(pPlayer):isDead()) then
				CreatureObject(pPlayer):sendSystemMessage("You watch on as the Baradium bomb detonates. Scouring the surrounding area of life")
				CreatureObject(pPlayer):setPosture(DEAD, 1, 1)
				CreatureObject(pPlayer):playEffect("clienteffect/restuss_event_city_explosion_personnel.cef", "")
				CreatureObject(pPlayer):playEffect("clienteffect/restuss_event_hanging_smoke.cef", "")
			end
		end
		for j = 0, 6, 3 do
			CreatureObject(pPlayer):inflictDamage(pBoss, j, CreatureObject(pPlayer):getMaxHAM(j) * 100, 1)
		end
	end
	--CreatureObject(pBoss):killArea(100)
	self:cleanUp(pBoss)
	self:destroyObject(pBoss)
	createEvent(25 * 1000, self.scriptName, "cleanupScene", nil, "")
	createEvent(120 * 1000, self.scriptName, "start", nil, "")
	broadcastGalaxy("all", "The tear in the force has not been cleansed. It will be vulnerable again in 2 minutes. Regroup now.")
end
