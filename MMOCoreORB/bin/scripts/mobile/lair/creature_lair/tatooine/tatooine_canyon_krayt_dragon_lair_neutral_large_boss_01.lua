tatooine_canyon_krayt_dragon_lair_neutral_large_boss_01 = Lair:new {
	mobiles = {{"canyon_krayt_dragon",1},{"juvenile_canyon_krayt_dragon",2}},
	bossMobiles = {{"krayt_dragon_adolescent",1}},
	spawnLimit = 3,
	buildingsVeryEasy = {"object/tangible/lair/base/poi_all_lair_bones_large.iff"},
	buildingsEasy = {"object/tangible/lair/base/poi_all_lair_bones_large.iff"},
	buildingsMedium = {"object/tangible/lair/base/poi_all_lair_bones_large.iff"},
	buildingsHard = {"object/tangible/lair/base/poi_all_lair_bones_large.iff"},
	buildingsVeryHard = {"object/tangible/lair/base/poi_all_lair_bones_large.iff"},
}

addLairTemplate("tatooine_canyon_krayt_dragon_lair_neutral_large_boss_01", tatooine_canyon_krayt_dragon_lair_neutral_large_boss_01)
