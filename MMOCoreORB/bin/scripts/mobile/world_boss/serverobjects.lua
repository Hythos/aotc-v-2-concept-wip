

includeFile("world_boss/world_anakin_skywalker_light.lua")
includeFile("world_boss/world_anakin_skywalker_balance.lua")
includeFile("world_boss/world_anakin_skywalker_dark.lua")
includeFile("world_boss/world_anakin_skywalker_emperor.lua")
includeFile("world_boss/world_padme.lua")
includeFile("world_boss/world_ahsoka_dark.lua")
includeFile("world_boss/world_luke.lua")
includeFile("world_boss/world_leia.lua")

includeFile("world_boss/world_clonetrooper_501.lua")
includeFile("world_boss/world_stormtrooper.lua")
includeFile("world_boss/world_clonetrooper_captain_501.lua")
