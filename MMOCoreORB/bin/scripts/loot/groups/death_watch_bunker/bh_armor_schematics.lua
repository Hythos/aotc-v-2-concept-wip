--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

bh_armor_schematics = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "bh_helmet_schematic", weight = 500000},
		{itemTemplate = "bh_leggings_schematic", weight = 500000},
		{itemTemplate = "bh_chestplate_schematic", weight = 500000},
		{itemTemplate = "bh_boots_schematic", weight = 500000},
		{itemTemplate = "bh_gloves_schematic", weight = 1000000},
		{itemTemplate = "bh_bracer_r_schematic", weight = 1500000},
		{itemTemplate = "bh_bracer_l_schematic", weight = 1500000},
		{itemTemplate = "bh_bicep_r_schematic", weight = 1500000},
		{itemTemplate = "bh_bicep_l_schematic", weight = 1500000},
		{itemTemplate = "bh_belt_schematic", weight = 1000000},
	}
}

addLootGroupTemplate("bh_armor_schematics", bh_armor_schematics)
