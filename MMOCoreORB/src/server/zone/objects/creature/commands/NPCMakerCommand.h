/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef NPCMAKERCOMMAND_H_
#define NPCMAKERCOMMAND_H_
#include "templates/customization/CustomizationIdManager.h"

class NPCMakerCommand : public QueueCommand {
public:

	NPCMakerCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		ManagedReference<PlayerObject*> ghost = creature->getPlayerObject();

		if (ghost == nullptr)
			return GENERALERROR;

		int adminLevelCheck = ghost->getAdminLevel();

		StringTokenizer args(arguments.toString());

		try {
			String command;
			if (args.hasMoreTokens()) {
				args.getStringToken(command);
				command = command.toLowerCase();

				if (adminLevelCheck != 15) {
					creature->sendSystemMessage("Sorry, the /npcmaker command requires administrator privileges.");
					return GENERALERROR;
				}

				if (command == "help") {
					creature->sendSystemMessage("/npcmaker save <filename> : Saves target's customization to a 'npc' file in the server directory.");
					creature->sendSystemMessage("/npcmaker procedure : Prints out directions on how to make an NPC CDF file with this command.");
				} else if (command == "save") {
					PrintNPC(creature, &args, target);
				}
				else if (command == "procedure") {
					creature->sendSystemMessage("==NPC TO CDF PROCEDURE==");
					creature->sendSystemMessage("1. Target yourself, or an NPC, and use the command /npcmaker save <filename>");
					creature->sendSystemMessage("2. Locate the .NPC file found in bin/custom_scripts/npcEditor/");
					creature->sendSystemMessage("3. Open Borrie's NPCMaker CDF Converter, available to download on ModTheGalaxy.com");
					creature->sendSystemMessage("4. Point the Converter's input directory path to the new NPC file you've created.");
					creature->sendSystemMessage("5. Designate the output directory in the Converter, and hit the button 'Convert File to CDF'");
					creature->sendSystemMessage("6. Assign the CDF to an NPC, and you've created a new NPC CDF!");
				}
				else {
					throw Exception(); // Used: /npcmaker <wrong>
				}
				
			}
		} catch (Exception& e) {
			creature->sendSystemMessage("Invalid arguments for npcmaker command. Help: /npcmaker help");
		}

		return SUCCESS;
	}

	void PrintNPC(CreatureObject* creature, StringTokenizer* args, const uint64& target) const {

		ManagedReference<SceneObject*> object = server->getZoneServer()->getObject(target, false);

		CreatureObject* targetMob = creature;

		if (object != nullptr) {
			if (object->isCreatureObject()) {
				targetMob = cast<CreatureObject*>(object.get());
			}
		}		

		String fileName = "";

		if (args->hasMoreTokens()) {
			args->getStringToken(fileName);
		}
		else {
			creature->sendSystemMessage("Error: /npcmaker save <filename> is missing a filename value argument.");
			return;
		}

		const WearablesDeltaVector* wearablesVector = targetMob->getWearablesDeltaVector();
		int size = wearablesVector->size();

		int gender = targetMob->getGender();
		CustomizationVariables* playerCustomVars = targetMob->getCustomizationVariables();

		StringBuffer text;

		for (int i = 0; i < size; i++) {
			TangibleObject* item = wearablesVector->get(i);
			CustomizationVariables* itemCustomVars = item->getCustomizationVariables();
			String appearance = item->getObjectTemplate()->getAppearanceFilename();
			if (appearance == "") continue;
			if (gender == 0) appearance = appearance.replaceFirst("_f.", "_m.");
			appearance = appearance.replaceFirst("appearance/", "appearance/mesh/");
			appearance = appearance.replaceFirst(".sat", ".lmg");
			text << "MESH " << appearance << "\n";
			int itemVarSize = itemCustomVars->getSize();

			for (int j = 0; j < itemVarSize; j++) {
				uint8 key = itemCustomVars->elementAt(j).getKey();
				int16 value = itemCustomVars->elementAt(j).getValue();
				String valueType = CustomizationIdManager::instance()->getCustomizationVariable(key);
				text << "WCSI " << valueType << "," << String::valueOf(value) << "\n";
			}
		}

		int playerVarSize = playerCustomVars->getSize();

		for (int k = 0; k < playerVarSize; k++) {
			uint8 key = playerCustomVars->elementAt(k).getKey();
			int16 value = playerCustomVars->elementAt(k).getValue();
			String valueType = CustomizationIdManager::instance()->getCustomizationVariable(key);

			text << "CSSI " << valueType << "," << String::valueOf(value) << "\n";
		}

		//Save to File

		if (fileName.isEmpty())
			throw Exception();

		File* file = new File("custom_scripts/npcEditor/" + fileName + ".npc");
		FileWriter* writer = new FileWriter(file, true); // true for appending new lines

		writer->writeLine(text.toString());

		writer->close();
		delete file;
		delete writer;

		creature->sendSystemMessage("Data written to bin/custom_scripts/npcEditor/" + fileName + ".npc!");
	}

};

#endif //NPCMAKERCOMMAND_H_
